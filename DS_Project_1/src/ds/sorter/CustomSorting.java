package ds.sorter;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import ds.fruit.Fruit;

public class CustomSorting {

	
	/**
	 * This method must sort the fruit alphabetically by their type. Fruits must be sorted 
	 *  in the order Apple, Banana, Grape, Pear. The weight and ripeness of a fruit should not
	 *  be considered. 
	 *  
	 * @param list The list to sort. 
	 * @return A sorted list. 
	 */
	public static List<Fruit> sortByType(List<Fruit> list) {
		//return null;
		
		//Sort the array by the fruit type
		return list.sort(new Comparator<Fruit>() {
			@Override
			public int compare(Fruit arg0, Fruit arg1) {
				return arg0.getType().compareTo(arg1.getType());
			}
		});
		

		//return list.sort(Comparator.comparing(Fruit::getType));
	}
	
	
	/**
	 * This method must sort the fruit by their ripeness value, from 100% ripe to 0% ripe. 
	 * The type and weight of the fruit should not be considered. 
	 * 
	 * @param list The fruit to sort
	 * @return	   A sorted list. 
	 */
	public static List<Fruit> sortByRipeness(List<Fruit> list) {
		//return null;
		
		//Sort the array by the fruit ripeness
		return list.sort(new Comparator<Fruit>() {
			@Override
			public int compare(Fruit arg0, Fruit arg1) {
				return arg0.getRipeness().compareTo(arg1.getRipeness());
			}});
		
		//return list.sort(Comparator.comparing(Fruit::getRipeness));
	}
}
