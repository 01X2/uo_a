package ds.fruit;

//Create the invalidfruitripeness exception
public class InvalidFruitRipenessException extends Exception { 
    InvalidFruitRipenessException(String errorMessage) {
       super(errorMessage);
   }
}