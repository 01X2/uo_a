package ds.fruit;

//Define our main class
public class Fruit {

    //Define the required variables
    public double weight;

    public enum TYPE {
        APPLE,
        PEAR,
        BANANA,
        GRAPE
      }
    public double ripeness;


    //Create getters and setters for the variables
    public double getWeight() {
        return this.weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public Object getType() {
        return this.getClass();
    };

    public double getRipeness() {
        return this.ripeness;
    }

    public void setRipeness(double ripeness) {
        this.ripeness = ripeness;
    }

    //Our toString override
    //
    //I didn't see a format for this so I'm guessing showing off all the variables is fine?
    //
    @Override
    public String toString() {
      return String.format("%s - Weight: %s, Ripeness: %s", this.getType(), this.getWeight(), this.getRipeness());
    };

    //Our equals override
    @Override
    public boolean equals(Object o) {
      if (this == o) {
        return true;
      }

      return false;
    };

    //Our hashcode override
    @Override
    public int hashCode() {
        return this.hashCode();
    };


    //Fruit constructor
    public Fruit(TYPE fruit, double weight, double ripeness) throws InvalidFruitWeightException, InvalidFruitRipenessException {

        //Check if weight is less than 0
        if (weight < 0) {
            throw new InvalidFruitWeightException("Weight supplied is less than or equal to 0");
        }

        //Check if ripeness is less than 0 or greater than 1
        if (ripeness < 0 || ripeness > 1) {
            throw new InvalidFruitRipenessException("Fruit ripeness falls out of 0-1");
        }

      }
}