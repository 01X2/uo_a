package ds.fruit;

//Create the invalidfruitweight exception
public class InvalidFruitWeightException extends Exception { 
    InvalidFruitWeightException(String errorMessage) {
       super(errorMessage);
   }
}